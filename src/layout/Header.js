import React from "react";

const Header = () => (
    <header className="header">
        <h1>Interview app</h1>
    </header>
);

export default Header;