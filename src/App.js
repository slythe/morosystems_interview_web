import React, {Component} from 'react';
import './sass/app.css';
import Homepage from "./pages/homepage/Homepage";
import Header from "./layout/Header";
import Footer from "./layout/Footer";
import Main from "./layout/Main";

class App extends Component {
    render() {
        return (
            <div className="app">
                <Header/>
                <Main>
                    <Homepage/>
                </Main>
                <Footer/>
            </div>
        );
    }
}

export default App;