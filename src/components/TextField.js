import React from 'react';
import PropTypes from "prop-types";

const TextField = ({handleChange, value, className}) => (
    <input value={value} className={`form-control ${className || ''}`} type="text" onChange={(e => handleChange(e.target.value))}/>
);

TextField.propTypes = {
    handleChange: PropTypes.func.isRequired,
    value: PropTypes.string,
    className: PropTypes.string,
};

export default TextField;