import React from 'react';
import PropTypes from "prop-types";

const ErrorMessage = ({message}) => (
    <div className="alert alert-danger">
        {message}
    </div>
);

ErrorMessage.propTypes = {
    message: PropTypes.string.isRequired,
};

export default ErrorMessage;