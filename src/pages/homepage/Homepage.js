import React, {Component} from 'react';
import UserTable from "./components/UserTable";
import UserForm from "./components/UserForm";
import request from "superagent"
import base64 from "base-64";
import ErrorMessage from "../../components/ErrorMessage";

class Homepage extends Component {

    constructor(props) {
        super(props);

        this.state = {users: [], error: null};
        this.deleteUser = this.deleteUser.bind(this);
        this.addUser = this.addUser.bind(this);
        this.editUser = this.editUser.bind(this);
    }

    componentDidMount() {
        request
            .get("/api/users/")
            .then(response =>  this.setState({users: response.body.data.users, error: null}))
            .catch(() => this.setState({error: "Data fetching failed"}));
    }

    deleteUser(id) {
        request
            .delete(`/api/users/${id}/`)
            .set('Authorization', `Basic ${base64.encode(`${process.env.REACT_APP_USERNAME}:${process.env.REACT_APP_PASSWORD}`)}`)
            .then(() =>  this.setState({users: this.state.users.filter(user => user.id !== id), error: null}))
            .catch(() => this.setState({error: `Deleting user with id ${id} failed`}));
    }

    addUser(name) {
        request
            .post(`/api/users/`, {name})
            .then(response =>  this.setState({users: [...this.state.users, response.body.data.user], error: null}))
            .catch(() => this.setState({error: `Adding user with name '${name}' failed`}));
    }

    editUser(id, name) {
        request
            .patch(`/api/users/${id}/`, {name})
            .then(response =>  {
                let users = [...this.state.users];
                users[users.findIndex(user => user.id === id)] = response.body.data.user;

                this.setState({users, error: null});
            })
            .catch(() => this.setState({error: `Editing user with id ${id} failed`}));
    }


    render() {
        return (
            <div>
                {this.state.error ? <ErrorMessage message={this.state.error}/> : null}
                <UserForm addUser={this.addUser}/>
                <UserTable users={this.state.users} editUser={this.editUser} deleteUser={this.deleteUser}/>
            </div>
        );
    }
}

export default Homepage;