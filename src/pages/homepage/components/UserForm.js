import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TextField from "../../../components/TextField";

class UserTable extends Component {
    constructor(props) {
        super(props);
        this.state = {name: ''}
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-10">
                    <TextField handleChange={(value) => this.setState({name: value})}/>
                </div>
                <div className="col-sm-1">
                    <button className="btn btn-success" onClick={() => this.props.addUser(this.state.name)}>Add</button>
                </div>
            </div>
        );
    }
}

UserTable.propTypes = {
    addUser: PropTypes.func.isRequired
};

export default UserTable;