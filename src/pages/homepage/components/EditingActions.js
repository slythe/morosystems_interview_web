import React from 'react';
import ReactTooltip from 'react-tooltip'
import PropTypes from "prop-types";

const EditingActions = ({user, cancel, save}) => (
    <div>
        <i data-tip data-for={`save-${user.id}`} className="icon-action fas fa-check" onClick={() => save(user.id)}/>
        <ReactTooltip id={`save-${user.id}`} aria-haspopup='true'>
            <span>Save</span>
        </ReactTooltip>
        <i data-tip data-for={`cancel-${user.id}`} className="icon-action fas fa-times-circle" onClick={() => cancel(user.id)}/>
        <ReactTooltip id={`cancel-${user.id}`} aria-haspopup='true'>
            <span>Cancel</span>
        </ReactTooltip>
    </div>
);

EditingActions.protoTypes = {
    save: PropTypes.func.isRequired,
    cancel: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
};

export default EditingActions;