import React from 'react';
import ReactTooltip from 'react-tooltip'
import PropTypes from "prop-types";

const Actions = ({user, editUser, deleteUser}) => (
    <div>
        <i data-tip data-for={`edit-${user.id}`} className="icon-action fas fa-pencil-alt" onClick={() => editUser(user)}/>
        <ReactTooltip  id={`edit-${user.id}`} aria-haspopup='true'>
            <span>Edit</span>
        </ReactTooltip>
        <i data-tip data-for={`delete-${user.id}`} className="icon-action fas fa-trash-alt" onClick={() => deleteUser(user.id)}/>
        <ReactTooltip  id={`delete-${user.id}`} aria-haspopup='true'>
            <span>Delete</span>
        </ReactTooltip>
    </div>
);

Actions.protoTypes = {
    deleteUser: PropTypes.func.isRequired,
    editUser: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
};

export default Actions;