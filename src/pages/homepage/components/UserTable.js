import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Actions from "./Actions";
import TextField from "../../../components/TextField";
import EditingActions from "./EditingActions";

class UserTable extends Component {


    constructor(props) {
        super(props);
        this.state = {editing: {}};
        this.editUser = this.editUser.bind(this);
        this.startEditing = this.startEditing.bind(this);
        this.stopEditing = this.stopEditing.bind(this);
        this.saveEditing = this.saveEditing.bind(this);
    }

    editUser(id, name) {
        this.setState({editing: {...this.state.editing, [id]: name}})
    }

    startEditing(user) {
        this.setState({editing: {...this.state.editing, [user.id]: user.name}})
    }

    stopEditing(userId) {
        let editing = {...this.state.editing};
        delete editing[userId];
        this.setState({editing})
    }

    saveEditing(userId){
        this.props.editUser(userId, this.state.editing[userId]);
        this.stopEditing(userId);
    }

    render() {
        return (
            <div>
                <table className="table table-hover table-users">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.users.map(user => (
                        this.state.editing.hasOwnProperty(user.id) ? (
                            <tr key={user.id}>
                                <td>{user.id}</td>
                                <td><TextField className="center" handleChange={value => this.editUser(user.id, value)} value={this.state.editing[user.id]}/></td>
                                <td><EditingActions user={user} save={this.saveEditing} cancel={this.stopEditing}/></td>
                            </tr>
                        ) : (
                            <tr key={user.id}>
                                <td>{user.id}</td>
                                <td>{user.name}</td>
                                <td><Actions user={user} editUser={this.startEditing} deleteUser={this.props.deleteUser}/></td>
                            </tr>
                            )
                    ))}
                    </tbody>
                </table>
            </div>
        );
    }
}

UserTable.propTypes = {
    users: PropTypes.array.isRequired,
    deleteUser: PropTypes.func.isRequired,
    editUser: PropTypes.func.isRequired,
};

export default UserTable;