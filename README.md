# Interview web

## Requirements
- Node 9.3
- yarn

## Installation
- `git clone git@bitbucket.org:slythe/morosystems_interview_web.git`
- `yarn install`

## Running
- `yarn run start`

## Configuration

Use `REACT_APP_USERNAME` and `REACT_APP_PASSWORD` in `.env.local` file ([documentation](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#adding-custom-environment-variables))

